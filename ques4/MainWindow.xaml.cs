﻿using Microsoft.Uii.Csr.Browser.Web;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf_Ques4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WorkerVM _vm;
        BackgroundWorker worker = new BackgroundWorker();
        public MainWindow()
        {
            InitializeComponent();
            _vm = (WorkerVM)Resources["vm"];
        }

        private void selectFolder_Click(object sender, RoutedEventArgs e)
        {
            _vm.SelectFolder();
        }
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            _vm.StartWorkerAsync();
        }
    }
}
