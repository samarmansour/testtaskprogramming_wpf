﻿using Microsoft.Uii.Csr.Browser.Web;
using Microsoft.Win32;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf_Ques4
{
    public class WorkerVM
    {
        private BackgroundWorker _worker;
        private int _iterations = 50;
        private string _output;
        private string _target;
        private object progressbar;

        public int Iteration
        {
            get { return _iterations; }
            set
            {
                if (_iterations != value)
                {
                    _iterations = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("Iterations"));
                }
            }
        }

        #region INotifyPropertyChanged Member
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }
        #endregion

        public WorkerVM()
        {
            _worker = new BackgroundWorker()
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            _worker.DoWork += WorkerDoWork;
            _worker.ProgressChanged += WorkerProgressChanged;
        }

        private void WorkerDoWork(object sender, DoWorkEventArgs e)
        {
            CopyFile(_output, _target);
        }

        private void WorkerProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressbar = e.ProgressPercentage;
        }

        public void SelectFolder()
        {
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog().ToString().Equals("OK"))
            {
                _output = open.FileName;
            }
        }

        public void CopyFile(string source, string des)
        {
            FileStream fsOut = new FileStream(des, FileMode.Create);
            FileStream fsIn = new FileStream(source, FileMode.Open);
            byte[] bt = new byte[1048756];
            int read;
            while ((read = fsIn.Read(bt, 0, bt.Length)) > 0)
            {
                fsOut.Write(bt,0,read);
                _worker.ReportProgress((int)(fsIn.Position * 100 / fsIn.Length));
            }
            fsIn.Close();
            fsOut.Close();
        }

        public void StartWorkerAsync()
        {
            _worker.RunWorkerAsync();
        }

    }
}
