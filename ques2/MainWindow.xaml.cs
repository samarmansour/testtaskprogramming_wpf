﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WpfGame_Ques2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Timer();
        }

        private void SafeInvoke(Action work)
        {
            if (Dispatcher.CheckAccess())
            {
                work.Invoke();
                return;
            }
            this.Dispatcher.BeginInvoke(work);
        }
       
        private Task Timer()
        {
            return Task.Run(() =>
            {
                for (int i = 30; i >= 0; i--)
                {
                    SafeInvoke(() => 
                    { 
                        lblTimer.Content = i;
                        if (i < 15)
                        {
                            lblTimer.Foreground = Brushes.Red;
                        }
                        //if (i==0)
                        //{
                        //    WrongAns();
                        //}
                    });
                    Thread.Sleep(500);
                }
            });
        }


        //Method for No Answer
        private void WrongAns()
        {
            btnChoice1.IsEnabled = false;
            btnChoice2.IsEnabled = false;
            btnChoice3.IsEnabled = false;
            btnChoice4.IsEnabled = false;
            Style style = new Style
            {
                TargetType = typeof(Window)
            };
            style.Setters.Add(new Setter(Window.BackgroundProperty, Brushes.Red));
            Application.Current.Resources["MyStyle"] = style;
        }

        private void CorrectAnswer()
        {
            btnChoice1.IsEnabled = false;
            btnChoice3.IsEnabled = false;
            btnChoice4.IsEnabled = false;
            Style style = new Style
            {
                TargetType = typeof(Window)
            };
            style.Setters.Add(new Setter(Window.BackgroundProperty, Brushes.LightGreen));
            Application.Current.Resources["MyStyle"] = style;
        }

        private void btnChoice1_Click(object sender, RoutedEventArgs e)
        {
            WrongAns();
        }

        private void btnChoice2_Click(object sender, RoutedEventArgs e)
        {
            CorrectAnswer();
            MessageBox.Show("Well Done!☺");
        }

        private void btnChoice3_Click(object sender, RoutedEventArgs e)
        {
            WrongAns();
        }

        private void btnChoice4_Click(object sender, RoutedEventArgs e)
        {
            WrongAns();
        }
    }
}
