﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TestTaskProgramming_WPF
{
    public class IntToStringConventer : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int maxValue = 200;
            var isInt = (int)value;
            if (isInt/100 * maxValue <= maxValue * 0.25)
            {
                return "Small";
            }
            else if (isInt / 100 * maxValue <= maxValue * 0.5)
            {
                return "Medium";
            }
            else if (isInt / 100 * maxValue <= maxValue * 0.75)
            {
                return "Large";
            }
            else
            {
                return "Extra Large";
            }
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
